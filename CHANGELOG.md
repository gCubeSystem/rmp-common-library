
# Changelog for workspace

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.9.0] - 2023-04-22

- Feature #25384: rmp-common-library ContextManager should read VO IDs as well

## [v2.8.6] - 2022-06-17

 - Fixed pom for HL portal removal

## [v2.8.5] - 2022-03-17

- Fix for #21652 Some keys contain the context and some other (the VRE contextes) contains in the context name.

## [v2.8.4] - 2020-12-14

- Fix Bug #20337 infrastructure-monitor: the searchById doesn't work anymore

## [v2.8.3] - 2020-09-25

- Ported to git and revised xqueries to support new Exists DB version


## [v1.0.0] - 2010-10-15

- First release, for other logs see distro/changelog.xml
