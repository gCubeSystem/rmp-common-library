package org.gcube.resourcemanagement.support.client.events;

import com.google.gwt.event.shared.EventHandler;

public interface SetScopeEventHandler extends EventHandler {
  void onSetScope(SetScopeEvent event);
}
