declare namespace ic = 'http://gcube-system.org/namespaces/informationsystem/registry';
for $profiles in collection('/db/Profiles/GHN')//Document/Data/ic:Profile/Resource
let $gcf-version := $profiles/Profile/GHNDescription/RunTimeEnv/Variable[Key/text() = 'gCF-version']/Value/text() | $profiles/Profile/GHNDescription/RunTimeEnv/Variable[Key/text() = 'SmartGears']/Value/text()
let $ghn-version := $profiles/Profile/GHNDescription/RunTimeEnv/Variable[Key/text() = 'GHN-distribution-version']/Value/text()  | $profiles/Profile/GHNDescription/RunTimeEnv/Variable[Key/text() = 'SmartGearsDistribution']/Value/text()
let $scopes := string-join( $profiles/Scopes//Scope/text(), ';')
let $subtype := $profiles/Profile/Site/Domain/text()
<RES_SUBTYPE ISdefault =''/> 
    return 
<RESOURCE/>
