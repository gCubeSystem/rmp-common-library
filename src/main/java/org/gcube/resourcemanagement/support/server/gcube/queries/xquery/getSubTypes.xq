declare namespace ic = 'http://gcube-system.org/namespaces/informationsystem/registry';
let $type := '<RES_TYPE ISdefault ='MetadataCollection'/>'
let $subtypes :=
    for $_profiles in collection('/db/Profiles/<RES_TYPE ISdefault ='MetadataCollection'/>')//Document/Data/ic:Profile/Resource
    let $elem := if ($type eq "Service")
            then $_profiles/Profile/Class
            else if ($type eq "RunningInstance")
            then $_profiles/Profile/ServiceClass
            else if ($type eq "GenericResource")
            then $_profiles/Profile/SecondaryType
            else if ($type eq "GHN")
            then $_profiles/Profile/Site/Domain
            else if ($type eq "MetadataCollection")
            then $_profiles/Profile/MetadataFormat/Name
            else if ($type eq "RuntimeResource")
            then $_profiles/Profile/Category
            else if ($type eq "Collection" and ($_profiles/Profile/IsUserCollection/string(@value) eq 'true'))
            then "User"
            else if ($type eq "Collection" and ($_profiles/Profile/IsUserCollection/string(@value) eq 'false'))
            then "System"
            else ""
    return $elem
<!--
    return $elem
 -->
for $subtype in distinct-values($subtypes)
return
     <SUBTYPE/>