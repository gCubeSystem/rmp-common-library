<Resource>
        {$profiles/ID}
        <Type>{$profiles/Type/text()}</Type>
        <SubType>{$subtype}</SubType>
        <Scopes>{$scopes}</Scopes>
        <Name>{$profiles/Profile/Name/text()}</Name>
        <Host>{$profiles/Profile/RunTime/HostedOn/text()}</Host>
</Resource>