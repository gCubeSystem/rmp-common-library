# Given a couple of (type, subtype) retrieves
# the list of all resources contained in such category.
# Parameters:  
#   RES_TYPE the main type of the resource
# This query is used by forms

declare namespace ic = 'http://gcube-system.org/namespaces/informationsystem/registry';
for $profiles in collection('/db/Profiles/<RES_TYPE ISdefault ='GHN' />'))//Document/Data/ic:Profile/Resource
let $ghn-name := $profiles/Profile/GHN/@UniqueID/string()
let $gcf-version := $profiles/Profile/GHNDescription/RunTimeEnv/Variable[Key/text() = 'gCF-version']/Value/text()
let $ghn-version := $profiles/Profile/GHNDescription/RunTimeEnv/Variable[Key/text() = 'GHN-distribution-version']/Value/text()
let $scopes := string-join( $profiles/Scopes//Scope/text(), ';')
let $subtype := 
        if ($profiles/Type eq "Service")
        then $profiles/Profile/Class/text()
        else if ($profiles/Type eq "RunningInstance")
        then $profiles/Profile/ServiceClass/text()
        else if ($profiles/Type eq "GenericResource")
        then $profiles/Profile/SecondaryType/text()
        else if ($profiles/Type eq "GHN")
        then $profiles/Profile/Site/Domain/text()
        else if ($profiles/Type eq "MetadataCollection")
        then $profiles/Profile/MetadataFormat/Name/text()
        else if ($profiles/Type eq "Collection" and ($profiles/Profile/IsUserCollection/string(@value) eq 'true'))
        then "User"
        else if ($profiles/Type eq "Collection" and ($profiles/Profile/IsUserCollection/string(@value) eq 'false'))
        then "System"
        else ""
    where $subtype eq '<RES_SUBTYPE ISdefault='isti.cnr.it'/>'
    return 
<RESOURCE/>