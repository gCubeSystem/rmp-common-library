<Resource>
        {$profiles/ID}
        <Type>{$profiles//Resource/Type/text()}</Type>
        <SubType>{$subtype}</SubType>
        <Scopes>{$scopes}</Scopes>
        <Name>{$profiles/Profile/Name/text()}</Name>
        <Description>{$profiles/Profile/Description/text()}</Description>
</Resource>