declare namespace ic = 'http://gcube-system.org/namespaces/informationsystem/registry';
for $profiles in collection('/db/Profiles/RunningInstance')//Document/Data/ic:Profile/Resource
let $docname := concat('/db/Profiles/GHN/',$profiles/Profile/GHN/@UniqueID/string())
let $_ghn-name := doc($docname)//Document/Data/ic:Profile/Resource/Profile/GHNDescription/Name/string()
let $ghn-name := if (empty($_ghn-name)) then $profiles/Profile/GHN/@UniqueID/string() else $_ghn-name
let $scopes := string-join( $profiles/Scopes//Scope/text(), ';')
let $subtype := $profiles/Profile/ServiceClass/text()
<RES_SUBTYPE ISdefault =''/> 
    return 
<RESOURCE/>
