<!--
    Notice: the <Resources> node must be removed in using the resource grid factory.
    -->
declare namespace ic = 'http://gcube-system.org/namespaces/informationsystem/registry';

let $_ri_type := '<RES_TYPE ISdefault ='RunningInstance'/>' 
let $_ri_id := '<RES_ID/>'

let $ri_docname := concat('/db/Profiles/RunningInstance/',$_ri_id)
let $ri := doc($ri_docname)//Document/Data/ic:Profile/Resource
let $ghn-id := $ri/Profile/GHN/@UniqueID/string()

let $ghn_docname := concat('/db/Profiles/GHN/',$ri/Profile/GHN/@UniqueID/string())
let $relatedghn := doc($ghn_docname)//Document/Data/ic:Profile/Resource
let $_ghn-name := $relatedghn/Profile/GHNDescription/Name/string()
let $ghn-name := if (empty($_ghn-name)) then $ghn-id else $_ghn-name 
return
<RESOURCE/>