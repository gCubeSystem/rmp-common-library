<!--
 PARAMETS:
    RES_ID the ID of the Service
 -->
     -->
declare namespace ic = 'http://gcube-system.org/namespaces/informationsystem/registry';
let $service := collection('/db/Profiles/Service')//Document/Data/ic:Profile/Resource[ID/text() eq '<RES_ID/>']
let $ServiceClass := $service/Profile/Class
let $ServiceName := $service/Profile/Name
let $ServiceVersion := $service/Profile/Version
let $riloop := collection('/db/Profiles/RunningInstance')//Document/Data/ic:Profile/Resource[Profile/ServiceClass/string() eq $ServiceClass and Profile/ServiceName/string() eq $ServiceName]
let $relatedris := 
    for $ri in $riloop
        let $ghn-id := $ri/Profile/GHN/@UniqueID/string()
        let $ghn := doc(concat('/db/Profiles/GHN',$ghn-id))//Document/Data/ic:Profile/Resource
    <!-- and $ri//Profile/Version/string() eq $ServiceVersion -->
    return 
<RESOURCE/>